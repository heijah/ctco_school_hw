
package ctco_1;

public class ctco_1 {
    
    public static int getResult(int[] inputArray)
    {
        
        int arrayLenght;    
        int currentTimesCounter     = 0;                        //Times current contestant has repeated
        int mostTimesCounter        = 0;                        //Most times one has repeated
        int currentLeader           = inputArray[0];            
        int currentContestant       = inputArray[0];            
      
        
        arrayLenght = inputArray.length;
       
        
        for (int i = 0; i < arrayLenght; i++)                   //Runs through every member of the array
        {
            if(inputArray[i] == currentContestant)              //Checks if this is a repeating value
            {
                currentTimesCounter++;                          //if yes - updates times repeating
            }
            else
            {
                if(currentTimesCounter >= mostTimesCounter)     //checks for a new record
                {   
                    currentLeader = inputArray[i-1];            //if new record - adds to the leaderbord
                    mostTimesCounter = currentTimesCounter;
                }
                currentContestant = inputArray[i];              //changes contestant and resets times appeared
                currentTimesCounter = 1;
            }
        }
        
        if(currentTimesCounter >= mostTimesCounter)             //checks the last contestant for a new record
        {   
            currentLeader = inputArray[arrayLenght-1];          //if new record - adds to the leaderbord
        }
        
        return currentLeader;
    }
    
    public static void main(String[] args) {

        int[] intArray = new int[] {1,1,1,1,2,2,3,3,3};
        
        //Array printout
        System.out.print("Given array: [" );
        for (int i = 0; i<=intArray.length -2; i++)
        {
            System.out.print(intArray[i] + ",");
        }
        System.out.println(intArray[intArray.length-1] + "]");
        
        
        System.out.print("Integer from the longest reccurring sequence within the array: " );
        int result = ctco_1.getResult(intArray);
        System.out.println(result);
        
    }
    
}
