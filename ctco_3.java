
package ctco_3;

public class ctco_3 {

    public static boolean checkBallanced(char selectedChar, char givenAnswer)
    {
        char neededAnswer;
        switch (selectedChar){
            case '(':
                neededAnswer = ')';
                break;
            case '[':
                neededAnswer = ']';
                break;
            case '{':
                neededAnswer = '}';
                break;
            default :
                return false;     
        }
        
        return neededAnswer == givenAnswer;
            
    }
    public static void main(String[] args) {
        
        boolean result;
        String inputString = "{()]";
        if((inputString.length() % 2) == 0)
        {
            for(int i = 0; i<=inputString.length()/2 -1; i++)
            {
                result = ctco_3.checkBallanced(inputString.charAt(i), inputString.charAt(inputString.length()-1-i)); 
                
                if(result != true)
                {
                    System.out.print("Unballanced");
                    break;
                }
                else if(i == inputString.length()/2 -1)
                {
                    System.out.print("Ballanced");
                }
            }
        }
        else
        {
            System.out.print("Unballanced");
        }
    }
    
}
