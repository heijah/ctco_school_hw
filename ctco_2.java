package ctco_2;

import java.util.Scanner;

public class ctco_2 {


    public static void drawTriangle(int baseLenght)
    {
        int numOfFloors;

        if ((baseLenght % 2) == 0)
        {
            numOfFloors = baseLenght/2;

            for(int i = 0; i < numOfFloors; i++)
            {
                for(int j = numOfFloors; j > i+1; j--)
                    System.out.print(" ");
                for(int y = 0; y <= i*2+1; y++)
                    System.out.print("*");
                System.out.print("\n");
            }
        }
        else
        {
            numOfFloors = baseLenght/2+1;
            for(int i = 0; i < numOfFloors; i++)
            {
                for(int j = numOfFloors;j>i;j--)
                    System.out.print(" ");
                for(int y = 0; y <= i*2; y++)
                    System.out.print("*");
                System.out.print("\n");
            }
        }
    }
    
    public static void main(String[] args) {
        
        Scanner in = new Scanner(System.in);
        
        System.out.print("Enter base lenght: ");
        try
        {
            int num = in.nextInt();
            ctco_2.drawTriangle(num);   
        }
        catch(Exception e)
        {
            System.out.print("Please restart program and enter integer value.");
        }


    }
}
